# README

**SUMMARY:** This guide will illustrate how to develop a new Elixir application using Docker from the very beginning.  You can use your host operating system and favorite editor for creating/modifying files, but the application will run in a docker container based off of an Elixir image.

*NOTE: we run a docker image as a container*

Prequisites:

 - Computer with running version of Docker ( 19.03.13+ )
 - Internet connection

1.  Create a project directory for the new app

```shell
cd ~/Documents/PROGRAMMING/Elixir
mkdir elx_demo
```

2. Create a Docker file for our app

```shell
vim Dockerfile


# MY DEMO APP - Elixix/Phoenix
#
# VERSION 1.0.0
#
# Build image example:
#   docker build -f Dockerfile -t elx-demo:1.0.0 .
#
# elixir:latest is 1.10.4 at the time of original creation
# -------------------------------------------------------------------
FROM elixir:1.10.4
MAINTAINER John F. Hogarty <hogihung@gmail.com>

RUN apt-get update -qq && apt-get install -y build-essential netcat && \ 
    mix local.hex --force && \
    mix local.rebar --force && \
    rm -rf /var/cache/apt

RUN mkdir -p /elx-demo
WORKDIR /elx-demo

COPY . .

RUN elixir -v

EXPOSE 4000
EXPOSE 4001

ENTRYPOINT /bin/bash -l


➜  elx_demo ls -la
total 8
drwxr-xr-x   3 jhogarty  staff    96 Nov  2 09:22 .
drwxr-xr-x  36 jhogarty  staff  1152 Nov  2 09:22 ..
-rw-r--r--   1 jhogarty  staff   605 Nov  2 09:22 Dockerfile
➜  elx_demo
```

3.  Build the applications docker image

```shell
docker build -f Dockerfile -t elx-demo:1.0.0 .

# Example:
➜  elx_demo docker build -f Dockerfile -t elx-demo:1.0.0 .
Sending build context to Docker daemon   2.56kB
Step 1/10 : FROM elixir:1.10.4
1.10.4: Pulling from library/elixir
e4c3d3e4f7b0: Extracting [=================================================> ]  49.81MB/50.4MB
101c41d0463b: Download complete
8275efcd805f: Download complete
751620502a7a: Downloading [====================>                              ]  21.52MB/51.83MB
0a5e725150a2: Downloading [====>                                              ]   16.1MB/192.3MB
e5453250980a: Downloading [=======>                                           ]  23.64MB/158.4MB
b1e49f9d409a: Waiting
9194bf7e9b3f: Waiting
59c8a493b406: Waiting

{--snip--}

Step 9/10 : EXPOSE 4001
 ---> Running in 40a010b209a9
Removing intermediate container 40a010b209a9
 ---> 865fe95eb982
Step 10/10 : ENTRYPOINT /bin/bash -l
 ---> Running in 4e7486682178
Removing intermediate container 4e7486682178
 ---> f8f2865cc0b3
Successfully built f8f2865cc0b3
Successfully tagged elx-demo:1.0.0
➜  elx_demo
```

4. Check for images:

```shell
➜  elx_demo docker images | grep elx-demo
elx-demo               1.0.0               f8f2865cc0b3        2 minutes ago       1.26GB
➜  elx_demo
```

5.  (OPTIONAL) Create alias(es) to assist with lazy development

```shell
vim ~/.zshrc.local  (choose appropriate file for your environment)

# alias for changing to our apps directory
alias elxdemo="cd ~/Documents/PROGRAMMING/Elixir/elx_demo"

# alias for MySql DB, Test environment
alias mysql-test="docker run -it --name mysql-test --hostname mysqldb-test --rm -d -e MYSQL_ROOT_PASSWORD=Supp0rt -p 7306:3306 -e MYSQL_DATABASE=demo_test -e MYSQL_USER=demo_user_test -e MYSQL_PASSWORD=Supp0rt mysql:5.7.27 --collation-server=latin1_general_cs"
#
# alias for MySql DB, Dev environment
alias mysql-dev="docker run -it --name mysql-dev --hostname mysqldb-dev --rm -d -e MYSQL_ROOT_PASSWORD=Supp0rt -p 9306:3306 -e MYSQL_DATABASE=demo_dev -e MYSQL_USER=demo_user_dev -e MYSQL_PASSWORD=Supp0rt mysql:5.7.27 --collation-server=latin1_general_cs"


# NOTE: if you want to use Postgres instead of MySql, use the following:
# alias for MySql DB, Test environment
alias pg-test="docker run -it --name pgsql-test --hostname pgsqldb-test --rm -d -p 7432:5432 -e POSTGRES_DATABASE=demo_test -e POSTGRES_USER=demo_user_test -e POSTGRES_PASSWORD=Supp0rt postgres:13.0"
#
# alias for MySql DB, Dev environment
alias pg-dev="docker run -it --name pgsql-dev --hostname pgsqldb-dev --rm -d -p 9432:5432 -e POSTGRES_DATABASE=demo_dev -e POSTGRES_USER=demo_user_dev -e POSTGRES_PASSWORD=Supp0rt postgres:13.0"


# NOTE: You need to start the Database Docker images before our application
#       alias for our application, linking to appropriate database(s)
#
# MySql version
alias dkr-demo='docker run -it --name=demo-app -h=elxr --rm --link mysql-test --link mysql-dev -p 9000:4000 -p 9001:4001 -v /Users/$USER/Documents/PROGRAMMING/Elixir/elx_demo:/elx-demo elx-demo:1.0.0'
#
# Posgres version
alias dkr-demo='docker run -it --name=demo-app -h=elxr --rm --link pgsql-test --link pgsql-dev -p 9000:4000 -p 9001:4001 -v /Users/$USER/Documents/PROGRAMMING/Elixir/elx_demo:/elx-demo elx-demo:1.0.0'
```

6.  Start the needed database docker containers

```shell
# Example:
➜  ~ docker run -it --name mysql-test --hostname mysqldb-test --rm -d -e MYSQL_ROOT_PASSWORD=Supp0rt -p 7306:3306 -e MYSQL_DATABASE=demo_test -e MYSQL_USER=demo_user_test -e MYSQL_PASSWORD=Supp0rt mysql:5.7.27 --collation-server=latin1_general_cs
386a12e2f5b1c404f1dcbfd2a4ddfa6bc10621579a1349c027863cca7309f617
➜  ~ docker run -it --name mysql-dev --hostname mysqldb-dev --rm -d -e MYSQL_ROOT_PASSWORD=Supp0rt -p 9306:3306 -e MYSQL_DATABASE=demo_dev -e MYSQL_USER=demo_user_dev -e MYSQL_PASSWORD=Supp0rt mysql:5.7.27 --collation-server=latin1_general_cs
1fd22c46f21bdd9b4afc5470482e2f172a558db147d386d6bdd043d667adb5ef
➜  ~ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
1fd22c46f21b        mysql:5.7.27        "docker-entrypoint.s…"   4 seconds ago       Up 3 seconds        33060/tcp, 0.0.0.0:9306->3306/tcp   mysql-dev
386a12e2f5b1        mysql:5.7.27        "docker-entrypoint.s…"   16 seconds ago      Up 15 seconds       33060/tcp, 0.0.0.0:7306->3306/tcp   mysql-test
➜  ~

NOTE: if you do not already have the database docker images on your local computer, docker should pull them down for you.
``` 

7.  Start our newly created application docker container

```shell
➜  ~ docker run -it --name=demo-app -h=elxr --rm --link mysql-test --link mysql-dev -p 9000:4000 -p 9001:4001 -v /Users/jhogarty/Documents/PROGRAMMING/Elixir/elx_demo:/elx-demo elx-demo:1.0.0
root@elxr:/elx-demo# pwd
/elx-demo
root@elxr:/elx-demo# ls -la
total 8
drwxr-xr-x 3 root root   96 Nov  2 14:22 .
drwxr-xr-x 1 root root 4096 Nov  2 14:58 ..
-rw-r--r-- 1 root root  605 Nov  2 14:22 Dockerfile
root@elxr:/elx-demo#

# NOTE: we can see the Dockerfile we created on our host computer is present in our container
```

8.  Confirm elixir version

```shell
# Example:
root@elxr:/elx-demo# ls -la
total 8
drwxr-xr-x 3 root root   96 Nov  2 14:22 .
drwxr-xr-x 1 root root 4096 Nov  2 14:58 ..
-rw-r--r-- 1 root root  605 Nov  2 14:22 Dockerfile
root@elxr:/elx-demo# elixir -v
Erlang/OTP 22 [erts-10.7.2.5] [source] [64-bit] [smp:3:3] [ds:3:3:10] [async-threads:1] [hipe]

Elixir 1.10.4 (compiled with Erlang/OTP 22)
root@elxr:/elx-demo#
```

9.  Install our dependencies (Hex and Phoenix) 

```
mix local.hex
mix archive.install hex phx_new 1.5.6

# Example:
root@elxr:/elx-demo# mix local.hex
Found existing entry: /root/.mix/archives/hex-0.20.6
Are you sure you want to replace it with "https://repo.hex.pm/installs/1.10.0/hex-0.20.6.ez"? [Yn] Y
* creating /root/.mix/archives/hex-0.20.6
root@elxr:/elx-demo#

root@elxr:/elx-demo# mix archive.install hex phx_new 1.5.6
Resolving Hex dependencies...
Dependency resolution completed:
New:
  phx_new 1.5.6
* Getting phx_new (Hex package)
All dependencies are up to date
Compiling 10 files (.ex)
Generated phx_new app
Generated archive "phx_new-1.5.6.ez" with MIX_ENV=prod
Are you sure you want to install "phx_new-1.5.6.ez"? [Yn]
* creating /root/.mix/archives/phx_new-1.5.6
root@elxr:/elx-demo#A

# Confirm version installed
root@elxr:/elx-demo# mix phx.new -v
Phoenix v1.5.6
root@elxr:/elx-demo#
```

10. See what phoenix commands are available

```shell
root@elxr:/elx-demo# mix help --search "phx"
mix local.phx    # Updates the Phoenix project generator locally
mix phx.new      # Creates a new Phoenix v1.5.6 application
mix phx.new.ecto # Creates a new Ecto project within an umbrella project
mix phx.new.web  # Creates a new Phoenix web project within an umbrella project
root@elxr:/elx-demo#

# Note: you can type mix phx.new to see what options are available
```

11. Create our Elixir-Phoenix application

```shell
# Note: will create an umbrella based application
mix phx.new elx_demo --live --umbrella --database mysql --verbose

# Example:
root@elxr:/elx-demo# ls -la
total 8
drwxr-xr-x 3 root root   96 Nov  2 14:22 .
drwxr-xr-x 1 root root 4096 Nov  2 14:58 ..
-rw-r--r-- 1 root root  605 Nov  2 14:22 Dockerfile
root@elxr:/elx-demo#
root@elxr:/elx-demo# mix phx.new elx_demo --live --umbrella --database mysql --verbose
* creating elx_demo_umbrella/.gitignore
* creating elx_demo_umbrella/config/config.exs
* creating elx_demo_umbrella/config/dev.exs
* creating elx_demo_umbrella/config/test.exs
* creating elx_demo_umbrella/config/prod.exs
* creating elx_demo_umbrella/config/prod.secret.exs
* creating elx_demo_umbrella/mix.exs
* creating elx_demo_umbrella/README.md
* creating elx_demo_umbrella/.formatter.exs
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/application.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/channels/user_socket.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/endpoint.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/router.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/telemetry.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/views/error_helpers.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/views/error_view.ex
* creating elx_demo_umbrella/apps/elx_demo_web/mix.exs
* creating elx_demo_umbrella/apps/elx_demo_web/README.md
* creating elx_demo_umbrella/apps/elx_demo_web/.gitignore
* creating elx_demo_umbrella/apps/elx_demo_web/test/test_helper.exs
* creating elx_demo_umbrella/apps/elx_demo_web/test/support/channel_case.ex
* creating elx_demo_umbrella/apps/elx_demo_web/test/support/conn_case.ex
* creating elx_demo_umbrella/apps/elx_demo_web/test/elx_demo_web/views/error_view_test.exs
* creating elx_demo_umbrella/apps/elx_demo_web/.formatter.exs
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/templates/layout/root.html.leex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/templates/layout/app.html.eex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/templates/layout/live.html.leex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/views/layout_view.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/live/page_live.ex
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/live/page_live.html.leex
* creating elx_demo_umbrella/apps/elx_demo_web/test/elx_demo_web/views/layout_view_test.exs
* creating elx_demo_umbrella/apps/elx_demo_web/test/elx_demo_web/live/page_live_test.exs
* creating elx_demo_umbrella/apps/elx_demo_web/lib/elx_demo_web/gettext.ex
* creating elx_demo_umbrella/apps/elx_demo_web/priv/gettext/en/LC_MESSAGES/errors.po
* creating elx_demo_umbrella/apps/elx_demo_web/priv/gettext/errors.pot
* creating elx_demo_umbrella/apps/elx_demo_web/assets/webpack.config.js
* creating elx_demo_umbrella/apps/elx_demo_web/assets/.babelrc
* creating elx_demo_umbrella/apps/elx_demo_web/assets/js/app.js
* creating elx_demo_umbrella/apps/elx_demo_web/assets/css/app.scss
* creating elx_demo_umbrella/apps/elx_demo_web/assets/package.json
* creating elx_demo_umbrella/apps/elx_demo_web/assets/static/favicon.ico
* creating elx_demo_umbrella/apps/elx_demo_web/assets/css/phoenix.css
* creating elx_demo_umbrella/apps/elx_demo_web/assets/static/images/phoenix.png
* creating elx_demo_umbrella/apps/elx_demo_web/assets/static/robots.txt
* creating elx_demo_umbrella/apps/elx_demo/lib/elx_demo/application.ex
* creating elx_demo_umbrella/apps/elx_demo/lib/elx_demo.ex
* creating elx_demo_umbrella/apps/elx_demo/test/test_helper.exs
* creating elx_demo_umbrella/apps/elx_demo/README.md
* creating elx_demo_umbrella/apps/elx_demo/mix.exs
* creating elx_demo_umbrella/apps/elx_demo/.gitignore
* creating elx_demo_umbrella/apps/elx_demo/.formatter.exs
* creating elx_demo_umbrella/apps/elx_demo/lib/elx_demo/repo.ex
* creating elx_demo_umbrella/apps/elx_demo/priv/repo/migrations/.formatter.exs
* creating elx_demo_umbrella/apps/elx_demo/priv/repo/seeds.exs
* creating elx_demo_umbrella/apps/elx_demo/test/support/data_case.ex

Fetch and install dependencies? [Yn] Y

{--snip--}

We are almost there! The following steps are missing:

    $ cd elx_demo_umbrella
    $ cd apps/elx_demo_web/assets && npm install && node node_modules/webpack/bin/webpack.js --mode development

Then configure your database in config/dev.exs and run:

    $ mix ecto.create

Start your Phoenix app with:

    $ mix phx.server

You can also run your app inside IEx (Interactive Elixir) as:

    $ iex -S mix phx.server

root@elxr:/elx-demo#
```

12. Review directory/files in the container and on our host computer

```shell
# In the container
root@elxr:/elx-demo# pwd
/elx-demo
root@elxr:/elx-demo# ls -la
total 8
drwxr-xr-x  4 root root  128 Nov  2 15:18 .
drwxr-xr-x  1 root root 4096 Nov  2 14:58 ..
-rw-r--r--  1 root root  605 Nov  2 14:22 Dockerfile
drwxr-xr-x 11 root root  352 Nov  2 15:19 elx_demo_umbrella
root@elxr:/elx-demo# ls -la elx_demo_umbrella/
total 28
drwxr-xr-x 11 root root  352 Nov  2 15:19 .
drwxr-xr-x  4 root root  128 Nov  2 15:18 ..
-rw-r--r--  1 root root   72 Nov  2 15:18 .formatter.exs
-rw-r--r--  1 root root  507 Nov  2 15:18 .gitignore
-rw-r--r--  1 root root   19 Nov  2 15:18 README.md
drwxr-xr-x  3 root root   96 Nov  2 15:19 _build
drwxr-xr-x  4 root root  128 Nov  2 15:18 apps
drwxr-xr-x  7 root root  224 Nov  2 15:18 config
drwxr-xr-x 31 root root  992 Nov  2 15:19 deps
-rw-r--r--  1 root root 1136 Nov  2 15:18 mix.exs
-rw-r--r--  1 root root 9775 Nov  2 15:19 mix.lock
root@elxr:/elx-demo#

root@elxr:/elx-demo# ls -la elx_demo_umbrella/apps/
total 0
drwxr-xr-x  4 root root 128 Nov  2 15:18 .
drwxr-xr-x 11 root root 352 Nov  2 15:19 ..
drwxr-xr-x  9 root root 288 Nov  2 15:18 elx_demo
drwxr-xr-x 10 root root 320 Nov  2 15:18 elx_demo_web
root@elxr:/elx-demo#


# On our host computer
➜  elx_demo pwd
/Users/jhogarty/Documents/PROGRAMMING/Elixir/elx_demo
➜  elx_demo ls -la
total 8
drwxr-xr-x   4 jhogarty  staff   128 Nov  2 10:18 .
drwxr-xr-x  36 jhogarty  staff  1152 Nov  2 09:22 ..
-rw-r--r--   1 jhogarty  staff   605 Nov  2 09:22 Dockerfile
drwxr-xr-x  11 jhogarty  staff   352 Nov  2 10:19 elx_demo_umbrella
➜  elx_demo ls -la elx_demo_umbrella
total 56
drwxr-xr-x  11 jhogarty  staff   352 Nov  2 10:19 .
drwxr-xr-x   4 jhogarty  staff   128 Nov  2 10:18 ..
-rw-r--r--   1 jhogarty  staff    72 Nov  2 10:18 .formatter.exs
-rw-r--r--   1 jhogarty  staff   507 Nov  2 10:18 .gitignore
-rw-r--r--   1 jhogarty  staff    19 Nov  2 10:18 README.md
drwxr-xr-x   3 jhogarty  staff    96 Nov  2 10:19 _build
drwxr-xr-x   4 jhogarty  staff   128 Nov  2 10:18 apps
drwxr-xr-x   7 jhogarty  staff   224 Nov  2 10:18 config
drwxr-xr-x  31 jhogarty  staff   992 Nov  2 10:19 deps
-rw-r--r--   1 jhogarty  staff  1136 Nov  2 10:18 mix.exs
-rw-r--r--   1 jhogarty  staff  9775 Nov  2 10:19 mix.lock
➜  elx_demo

➜  elx_demo ls -la elx_demo_umbrella/apps/
total 0
drwxr-xr-x   4 jhogarty  staff  128 Nov  2 10:18 .
drwxr-xr-x  11 jhogarty  staff  352 Nov  2 10:19 ..
drwxr-xr-x   9 jhogarty  staff  288 Nov  2 10:18 elx_demo
drwxr-xr-x  10 jhogarty  staff  320 Nov  2 10:18 elx_demo_web
➜  elx_demo
```

13. Move our original Docker file into the apps generated directory

```shell
root@elxr:/elx-demo# ls
Dockerfile  elx_demo_umbrella
root@elxr:/elx-demo#

mv Dockerfile elx_demo_umbrella

# Example
root@elxr:/elx-demo# ls -la
total 4
drwxr-xr-x  3 root root   96 Nov  2 15:40 .
drwxr-xr-x  1 root root 4096 Nov  2 14:58 ..
drwxr-xr-x 12 root root  384 Nov  2 15:40 elx_demo_umbrella
root@elxr:/elx-demo# ls -la elx_demo_umbrella/
total 32
drwxr-xr-x 12 root root  384 Nov  2 15:40 .
drwxr-xr-x  3 root root   96 Nov  2 15:40 ..
-rw-r--r--  1 root root   72 Nov  2 15:18 .formatter.exs
-rw-r--r--  1 root root  507 Nov  2 15:18 .gitignore
-rw-r--r--  1 root root  605 Nov  2 14:22 Dockerfile
-rw-r--r--  1 root root   19 Nov  2 15:18 README.md
drwxr-xr-x  3 root root   96 Nov  2 15:19 _build
drwxr-xr-x  4 root root  128 Nov  2 15:18 apps
drwxr-xr-x  7 root root  224 Nov  2 15:18 config
drwxr-xr-x 31 root root  992 Nov  2 15:19 deps
-rw-r--r--  1 root root 1136 Nov  2 15:18 mix.exs
-rw-r--r--  1 root root 9775 Nov  2 15:19 mix.lock
root@elxr:/elx-demo#
```

14. Commit application to version control (Gitlab/Github/BitBucket)

```shell
# From the host computer
➜  elx_demo_umbrella pwd
/Users/jhogarty/Documents/PROGRAMMING/Elixir/elx_demo/elx_demo_umbrella
➜  elx_demo_umbrella ls -la
total 64
drwxr-xr-x  12 jhogarty  staff   384 Nov  2 10:40 .
drwxr-xr-x   3 jhogarty  staff    96 Nov  2 10:40 ..
-rw-r--r--   1 jhogarty  staff    72 Nov  2 10:18 .formatter.exs
-rw-r--r--   1 jhogarty  staff   507 Nov  2 10:18 .gitignore
-rw-r--r--   1 jhogarty  staff   605 Nov  2 09:22 Dockerfile
-rw-r--r--   1 jhogarty  staff    19 Nov  2 10:18 README.md
drwxr-xr-x   3 jhogarty  staff    96 Nov  2 10:19 _build
drwxr-xr-x   4 jhogarty  staff   128 Nov  2 10:18 apps
drwxr-xr-x   7 jhogarty  staff   224 Nov  2 10:18 config
drwxr-xr-x  31 jhogarty  staff   992 Nov  2 10:19 deps
-rw-r--r--   1 jhogarty  staff  1136 Nov  2 10:18 mix.exs
-rw-r--r--   1 jhogarty  staff  9775 Nov  2 10:19 mix.lock
➜  elx_demo_umbrella

git init
git add .
git commit -m 'Initial commit'
```

15. In our docker container, now that we are in the applications directory, we can now see all the phoenix tasks available

```shell
root@elxr:/elx-demo/elx_demo_umbrella# mix help | grep phx
mix local.phx          # Updates the Phoenix project generator locally
mix phx                # Prints Phoenix help information
mix phx.digest         # Digests and compresses static files
mix phx.digest.clean   # Removes old versions of static assets.
mix phx.gen.cert       # Generates a self-signed certificate for HTTPS testing
mix phx.gen.channel    # Generates a Phoenix channel
mix phx.gen.context    # Generates a context with functions around an Ecto schema
mix phx.gen.embedded   # Generates an embedded Ecto schema file
mix phx.gen.html       # Generates controller, views, and context for an HTML resource
mix phx.gen.json       # Generates controller, views, and context for a JSON resource
mix phx.gen.live       # Generates LiveView, templates, and context for a resource
mix phx.gen.presence   # Generates a Presence tracker
mix phx.gen.schema     # Generates an Ecto schema and migration file
mix phx.gen.secret     # Generates a secret
mix phx.new            # Creates a new Phoenix v1.5.6 application
mix phx.new.ecto       # Creates a new Ecto project within an umbrella project
mix phx.new.web        # Creates a new Phoenix web project within an umbrella project
mix phx.routes         # Prints all routes
mix phx.server         # Starts applications and their servers
root@elxr:/elx-demo/elx_demo_umbrella#
```

## REVIEW

At this point we now have our new application, written using Elixir and Phoenix, running in a docker environment.  We may also have our database running in docker.

```shell
➜  ~ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                            NAMES
46f4e1933726        elx-demo:1.0.0      "/bin/sh -c '/bin/ba…"   47 minutes ago      Up 47 minutes       0.0.0.0:9000->4000/tcp, 0.0.0.0:9001->4001/tcp   demo-app
1fd22c46f21b        mysql:5.7.27        "docker-entrypoint.s…"   51 minutes ago      Up 51 minutes       33060/tcp, 0.0.0.0:9306->3306/tcp                mysql-dev
386a12e2f5b1        mysql:5.7.27        "docker-entrypoint.s…"   52 minutes ago      Up 52 minutes       33060/tcp, 0.0.0.0:7306->3306/tcp                mysql-test
➜  ~
```

At this point you can use your favorite editor on the host computer to make the needed configuration settings to get your application talking with the database.

Once configured, you then should be able to run your database migrations, if needed, and spin up your application for developing and/or running your test suite.

All application specific commands will be executed in the docker container.  Any files that require editting can be done on your host computer.

